FROM python:3.7-alpine
LABEL maintainer="Fabio Todaro <fbregist@gmail.com>"

ARG BUILD_DATE
ARG DOCKER_REPO
ARG VERSION
ARG VCS_REF

LABEL org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name=$DOCKER_REPO \
      org.label-schema.version=$VERSION \
      org.label-schema.description="Gitlab Registry Cleaner" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/SharpEdgeMarshall/gitlab-registry-cleaner"

ENV PIP_NO_CACHE_DIR=off \
    PYTHON_PIP_VERSION=19.2.1 \
    PYTHON_POETRY_VERSION=0.12.16

RUN adduser -D python

RUN apk --no-cache add \
    curl

# Install pip and poetry
RUN pip install -U \
    pip==${PYTHON_PIP_VERSION} && \
    curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | \
    python - --version=${PYTHON_POETRY_VERSION} --yes

# Config poetry
ENV PATH="${PATH}:/root/.poetry/bin"
RUN poetry config settings.virtualenvs.create false

WORKDIR /home/python/

COPY --chown=python . /home/python

COPY certs/ /usr/local/share/ca-certificates/
RUN update-ca-certificates

# Install dependencies
RUN poetry install --no-dev --no-interaction

USER python

ENV REQUESTS_CA_BUNDLE="/etc/ssl/certs/ca-certificates.crt"

CMD ["gitlab-registry-cleaner", "--help"]
