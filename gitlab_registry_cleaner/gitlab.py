import re
from urllib.parse import urlparse
import logging

import gitlab

class GitlabRegistryCleaner:
    def __init__(
        self,
        api_url,
        token,
        dry_run
    ):
        _gl_parsed_url = urlparse(api_url)
        _gl_url = _gl_parsed_url.scheme + '://' + _gl_parsed_url.netloc

        self._gl = gitlab.Gitlab(_gl_url, private_token=token)
        self._dry_run = dry_run

    def clean_registry_repositories(self, project_id, repository_name, tag_name, keep_n, older_than):
        
        try:
            project = self._gl.projects.get(project_id)
            logging.info(f'Project: {project.name}')
        except gitlab.exceptions.GitlabGetError:
            logging.critical(f'Unable to find project with ID "{project_id}""')
            return

        try:
            repositories = project.repositories.list()
        except gitlab.exceptions.GitlabListError:
            logging.critical(f'Unable to find repositories')
            logging.critical(f'Is Gitlab Registry enabled for project "{project.name}"?')
            return

        logging.info(f'Found {len(repositories)} repositories')

        for repository in repositories:
            if re.search(repository_name, repository.name):
                logging.info(f'Cleaning "{repository.name}" repository...')
                if not self._dry_run:
                    try:
                        repository.tags.delete_in_bulk(name_regex=tag_name, keep_n=keep_n, older_than=older_than)
                        logging.info(f'Cleaned "{repository.name}" using: tag_name="{tag_name}" keep_n="{keep_n}" older_than="{older_than}"')
                    except gitlab.exceptions.GitlabDeleteError as e:
                        logging.critical(f'Unable to clean "{repository.name}" using: tag_name="{tag_name}" keep_n="{keep_n}" older_than="{older_than}"')
                        logging.critical(e.error_message)
                else:
                    logging.info(f'DRY-RUN Would DELETE all images tags:')
                    logging.info(f'\tMatching tag name "{tag_name}"')                    
                    logging.info(f'\tOlder than "{older_than}"')
                    logging.info(f'\tKeeping last "{keep_n}" images')
